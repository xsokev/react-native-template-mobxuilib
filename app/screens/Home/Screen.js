import React from 'react';
import {observer, inject} from 'mobx-react/native';
import { View, Text, Button } from 'react-native-ui-lib';

import {colors, width, height} from '../../util/styles';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
}))
@observer
export default class Home extends React.Component {
  static navigationOptions = {
    title: 'Home'
  };
  render(){
    const {primary, primaryText, navigation} = this.props;
    return (
      <View flex center>
        <Text text20 dark20 center>Home</Text>
        <Button text70 white background-primary label="About" onPress={() => navigation.navigate('About')} />
      </View>
    )
  }
}
