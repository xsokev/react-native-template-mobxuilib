import { observable, action, computed, observe } from 'mobx';
import { APPLICATION_TITLE } from '../../config';

export default class Store {
  ui = null;

  constructor(ui) {
    this.ui = ui;
  }
}
