import {Screen as HomeScreen} from './screens/Home';
import {Screen as AboutScreen} from './screens/About';

export default {
  Home: { screen: HomeScreen },
  About: { screen: AboutScreen },
}
