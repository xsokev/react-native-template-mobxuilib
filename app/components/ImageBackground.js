import React from 'react';
import {View, Image} from 'react-native-ui-lib';
import {width, height} from '../util/styles';

export default ({children, source, mode}) => {
  const imageStyles = {
    position: 'absolute',
    left: 0, 
    right: 0,
    top: 0,
    bottom: 0,
    width, height
  }
  return (
    <View flex>
      <Image style={imageStyles} source={source} resizeMode={mode || 'cover'} />
      {children}
    </View>
  )
}
