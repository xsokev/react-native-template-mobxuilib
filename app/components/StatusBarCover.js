import React from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {width} from '../util/styles';

const styles = StyleSheet.create({
  cover: {
    height: Platform.OS === 'ios' ? 20 : 25,
    width: width,
    zIndex: 10,
    position: 'absolute',
    top: 0,
    left: 0
  }
})

export default ({color}) => {
  return (<View style={[styles.cover, {backgroundColor: color || 'transparent'}]}></View>)
}
