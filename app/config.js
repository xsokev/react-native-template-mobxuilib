const App = require('../app.json');

export const APPLICATION_TITLE    = "React Native Mobx App";
export const APPLICATION_PREFIX   = App.name;
export const SESSIONTIMEOUT       = 10 * 60 * 1000;
export const DATE_FORMAT          = "M/D/YYYY";
export const DATE_TIME_FORMAT     = "M/D/YYYY h:mm:ss A";
export const TIME_FORMAT          = "h:mma";

export const BASEURL              = "https://api.kevinandre.com";
export const API_KEY              = '';
export const REQUEST_TIMEOUT      = 20000;
export const API_EVENTS           = ()   => `${BASEURL}/api/events`;
export const API_EVENT            = (id) => `${BASEURL}/api/events/id/${id}`;

export const DEFAULT_FILTER_PLACEHOLDER = "filter events";
