import {useStrict} from 'mobx';

import UiStore from "./Ui";
import ThemeStore from "./Theme";

import {Store as HomeStore} from "../screens/Home";
import {Store as AboutStore} from "../screens/About";

import * as apiTransport from './transports/api';
import * as storageTransport from "./transports/storage";

const theme = new ThemeStore();
const ui = new UiStore(theme, storageTransport);
const home = new HomeStore(apiTransport, storageTransport, null, ui);
const about = new AboutStore(apiTransport, storageTransport, null, ui);

export {
  ui,
  theme,
  home,
  about,
}
