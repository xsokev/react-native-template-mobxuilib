import {NetInfo, Platform} from 'react-native';
import {observable, action, computed} from 'mobx';

export default class UiStore {
  theme = null;
  storage = null;
  @observable isConnected = true;
  @observable transparentHeader = true;
  @observable storedState = true;

  constructor(theme, storage){
    this.theme = theme;
    this.storage = storage;
    NetInfo.isConnected.fetch().then(isConnected => {
      this.setIsConnected(isConnected);
    });
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      isConnected => {
        this.setIsConnected(isConnected);
      }
    );
    this.storage && this.storage.load('storedState').then(storedata => this.storedState = storedata ? false : true);
  }
  @action setIsConnected(v){
    this.isConnected = v;
  }
  @action setStoredState(v){
    this.storedState = v;
    this.storage && this.storage.save('storedState', v.toString());
  }
  @computed get primaryHeader(){
    return this.transparentHeader ? {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    } : {
      backgroundColor: this.theme.primary,
      marginTop: Platform.OS === 'ios' ? 0 : 25
    }
  }
  @computed get primaryStatus(){
    return this.transparentHeader ? 'transparent' : this.theme.dark;
  }
}
