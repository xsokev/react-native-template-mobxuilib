import { StatusBar, Platform } from 'react-native';
import {observable, action, computed} from 'mobx';
import {Colors} from 'react-native-ui-lib';
import {colors, shade, textFromHex} from '../util/styles';

const adjustStatusBar = (color) => {
  StatusBar.setHidden(false);
  if(color === colors.clear){
    Platform.OS === 'android' && StatusBar.setTranslucent(true);
    StatusBar.setBarStyle('light-content');
  } else {
    Platform.OS === 'android' && StatusBar.setTranslucent(false);
    Platform.OS === 'android' && StatusBar.setBackgroundColor(shade(color, 1));
    StatusBar.setBarStyle(textFromHex(color) === colors.black ? 'dark-content' : 'light-content');
  }
}

export default class ThemeStore {
  @observable primary = colors.blue600;
  @observable accent = colors.amber500;
  @observable background = colors.white;

  constructor(){
    adjustStatusBar(this.primary);
    Colors.loadColors({
      primary: this.primary,
      accent: this.accent,
    });    
  }

  @action setPrimary(v){
    this.primary = v;
    adjustStatusBar(this.primary);
  }
  @action setAccent(v){
    this.accent = v;
  }
  @action setBackground(v){
    this.background = v;
  }

  @computed get dark() {
    return colors.grey900;
  }
  @computed get primaryText() {
    return this.primary === colors.clear ? colors.clear : textFromHex(this.primary);
  }
  @computed get accentText() {
    return textFromHex(this.accent);
  }
}
