# README #

This is a simple React Native Template with Mobx, React Navigation, Wix React Native UI Lib. Includes base application with storage, offline indicators, and theming through stores..

## Installation ##

    react-native init MyApp --template https://bitbucket.org/xsokev/react-native-template-mobxuilib

### NOTE ###

In order for the app to properly run, a few files have to be modified. The package react-native-ui-lib has a few references to React.PropTypes which is no longer supported. Changes have to be made to the following files in the directory 

> node_modules/react-native-ui-lib


* ./src/components/pageControl/index.js
	* change "React.PropTypes" to "PropTypes"
* ./src/components/screensComponents/loaderScreen/index.js
	* change "React.PropTypes" to "PropTypes"
* ./src/components/stepper/index.js
	* add "import PropTypes from 'prop-types'";
	* change "React.PropTypes" to "PropTypes"
* ./src/components/stepper/StepperButton.js
	* add "import PropTypes from 'prop-types'";
	* change "React.PropTypes" to "PropTypes"
* ./index.js
	* change "./dist/index" to "./src/index"